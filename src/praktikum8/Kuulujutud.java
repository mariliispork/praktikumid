package praktikum8;

import praktikum6.Meetodid;

public class Kuulujutud {

	public static void main(String[] args) {

		String[] naised = { "Juta", "Mari", "Kati" };
		String[] mehed = { "Mart", "Mati", "Joonas" };
		String[] tegevused = { "Räägivad", "Jooksevad", "Tantsivad" };

		System.out.format("%s ja %s %s.", suvalineElement(naised), suvalineElement(mehed), suvalineElement(tegevused));

	}

	private static String suvalineElement(String[] s6nad) {
		// s6nad.length
		int suvalineIndeks = Meetodid.suvalineArv(0, s6nad.length - 1);
		return s6nad[suvalineIndeks];
	}

}
