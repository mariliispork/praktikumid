package praktikum14;

import java.util.ArrayList;

public class FailiKeskmine {

	public static void main(String[] args) {
		
		// 1. loeme faili sisu saame ArrayList<String> <-- meetod olemas
		ArrayList<String> failiSisu = FailiLugeja.loeFail("numbrid.txt");
		
		// 2. ArrayList<String> -> ArrayList<Double> <-- eraldi meetod!
		ArrayList<Double> numbrid = teeNumbriteks(failiSisu);
		System.out.println(numbrid);
		
		// 3. Arvutad keskmise <-- eraldi meetod!
		double keskmine = arvutaKeskmine(numbrid);
		System.out.println("Nende arvude keskmine on: " + keskmine);

	}

	private static double arvutaKeskmine(ArrayList<Double> numbrid) {
		double summa = 0;
		for (Double nr : numbrid) {
			summa += nr;
		}
		return summa / numbrid.size();
	}

	public static ArrayList<Double> teeNumbriteks(ArrayList<String> read) {
		ArrayList<Double> numbrid = new ArrayList<Double>();
		for (String rida : read) {
			try {
				double nr = Double.parseDouble(rida);
				numbrid.add(nr);
			} catch (NumberFormatException e) {
				System.out.println("Ei oska seda numbriks teha: " + rida);
			}
		}
		return numbrid;
	}

}