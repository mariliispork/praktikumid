package praktikum9;
import com.sun.org.apache.xml.internal.security.keys.keyresolver.implementations.PrivateKeyResolver;

import lib.TextIO;

public class SuuredT2hedSidekriipsud {

	public static void main(String[] args) {

		System.out.println("Sisesta sõna: ");
		String sona = TextIO.getlnString();
		System.out.println(t66tlus(sona));
		
	}
	

	private static String t66tlus(String s6na) {
		String t66deldud = "";
		for (int i = 0; i < s6na.length(); i++) {
			if (i > 0) {
				t66deldud += "-";
			}
			t66deldud += s6na.toUpperCase().charAt(i);
		}		
		return t66deldud;
	}

}
