package praktikum9;

import java.awt.peer.SystemTrayPeer;

import praktikum1.TextIO;

public class Palindroom {

	public static void main(String[] args) {
		
		System.out.println("Palun sisesta sõna");
		String s6na = TextIO.getlnString();
		
		if (onPalindroom(s6na)) {
			System.out.println("See s6na on palindroom");
		} else {
			System.out.println("Tegemist ei ole palindroomiga");
			
		}

	}

	private static boolean onPalindroom(String s6na) {
		
		return s6na.equals(tagurpidi(s6na));
	}

	
	public static String tagurpidi(String oigetpidi) {
		String tagurpidi = "";
		for (int i = oigetpidi.length() - 1; i >= 0; i--) {
			tagurpidi += oigetpidi.charAt(i);
		}
		return tagurpidi;
	}
}
